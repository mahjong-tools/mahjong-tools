module Models.Suit where

import Data.List (elemIndex)
import Data.Maybe (fromMaybe)

data Suit = M | P | S | Z deriving (Eq)

instance Show Suit where
  show M = "m"
  show P = "p"
  show S = "s"
  show Z = "z"

-- |
-- >>> fmap (\s -> readMaybe s :: Maybe Suit) ["m", "p", "s", "z", "a"]
-- [Just m,Just p,Just s,Just z,Nothing]
instance Read Suit where
  readsPrec _ value = tryParse value
    where
      tryParse [] = []
      tryParse ('m' : xs) = [(M, xs)]
      tryParse ('p' : xs) = [(P, xs)]
      tryParse ('s' : xs) = [(S, xs)]
      tryParse ('z' : xs) = [(Z, xs)]
      tryParse _ = []

instance Ord Suit where
  compare s1 s2
    | ix1 < ix2 = LT
    | ix1 > ix2 = GT
    | otherwise = EQ
    where
      order = [M, P, S, Z]
      ix1 = fromMaybe (-1) $ elemIndex s1 order
      ix2 = fromMaybe (-1) $ elemIndex s2 order

countOfASuit :: Suit -> Int
countOfASuit M = 9
countOfASuit P = 9
countOfASuit S = 9
countOfASuit Z = 7

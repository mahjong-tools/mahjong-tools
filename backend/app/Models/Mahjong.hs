{-# LANGUAGE OverloadedStrings #-}

module Models.Mahjong where

import Data.List ((\\))
import Models.Suit (Suit (M, P, S, Z), countOfASuit)
import Models.Tile (Tile (Tile), Tiles (Tiles))

mahjongs :: [Tile]
mahjongs = tilesOfASuit M ++ tilesOfASuit P ++ tilesOfASuit S ++ tilesOfASuit Z

tilesOfASuit :: Suit -> [Tile]
tilesOfASuit s = [Tile (i, s) | i <- [1 .. (countOfASuit s)]] >>= replicate 4

newtype TileRemaining = TileRemaining (Tile, Int)

--- >>> ([1, 1, 1, 2, 2, 2] \\ [1, 2, 2]) \\ []
-- [1,1,2]

findRemaining :: Tiles -> Tile -> TileRemaining
findRemaining (Tiles remainingTiles) tile = TileRemaining (tile, length $ filter (== tile) remainingTiles)

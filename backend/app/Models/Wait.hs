module Models.Wait where

import Models.Tile (Tile)

data Wait
  = SequenceWaitForTwoSides Tile Tile Tile Tile
  | SequenceWaitForOneSide Tile Tile Tile
  | SequenceWaitForMiddle Tile Tile Tile
  | TripletWait Tile Tile Tile
  deriving (Eq, Ord)

instance Show Wait where
  show (SequenceWaitForTwoSides x y a b) = show x ++ show y ++ "(" ++ show a ++ show b ++ ")"
  show (SequenceWaitForOneSide x y a) = show x ++ show y ++ "(" ++ show a ++ ")"
  show (SequenceWaitForMiddle x y a) = show x ++ show y ++ "(" ++ show a ++ ")"
  show (TripletWait x y a) = show x ++ show y ++ "(" ++ show a ++ ")"

data PairWait = PairWait Tile Tile deriving (Eq, Ord)

instance Show PairWait where
  show (PairWait x a) = show x ++ "(" ++ show a ++ ")"

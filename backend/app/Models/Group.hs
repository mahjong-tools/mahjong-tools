module Models.Group where

import Models.Tile (Tile)

data Group = Group Tile Tile Tile deriving (Eq, Ord)

instance Show Group where
  show (Group x y z) = show x ++ show y ++ show z

data Pair = Pair Tile Tile deriving (Eq, Ord)

instance Show Pair where
  show (Pair x y) = show x ++ show y

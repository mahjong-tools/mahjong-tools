module Models.Tile where

import Control.Monad (join)
import Models.Suit (Suit (Z), countOfASuit)
import Text.Read (readMaybe)

newtype Tile = Tile (Int, Suit) deriving (Eq)

instance Show Tile where
  show (Tile (i, s)) = show i ++ show s

-- |
-- >>> fmap (\u -> readMaybe u :: Maybe Tile) ["1m", "9m", "1s", "9s", "1p", "9p", "1z", "7z", "9z"]
-- [Just 1m,Just 9m,Just 1s,Just 9s,Just 1p,Just 9p,Just 1z,Just 7z,Nothing]
instance Read Tile where
  readsPrec _ value = tryParse value
    where
      tryParse [] = []
      tryParse (maybeI : maybeU : xs) = case readMaybe [maybeI] :: Maybe Int of
        Nothing -> []
        Just i -> case readMaybe [maybeU] :: Maybe Suit of
          Nothing -> []
          Just u ->
            if i `elem` [1 .. (countOfASuit u)]
              then [(Tile (i, u), xs)]
              else do
                []
      tryParse _ = []

instance Ord Tile where
  compare (Tile (i1, s1)) (Tile (i2, s2))
    | s1 < s2 = LT
    | s1 > s2 = GT
    | otherwise = compare i1 i2

getSuccessorTile :: Tile -> Maybe Tile
getSuccessorTile (Tile (_, Z)) = Nothing
getSuccessorTile (Tile (9, _)) = Nothing
getSuccessorTile (Tile (i, s)) = Just $ Tile (i + 1, s)

getPrevTile :: Tile -> Maybe Tile
getPrevTile (Tile (_, Z)) = Nothing
getPrevTile (Tile (1, _)) = Nothing
getPrevTile (Tile (i, s)) = Just $ Tile (i - 1, s)

newtype Tiles = Tiles {getTiles :: [Tile]}

instance Show Tiles where
  show (Tiles tiles) = show =<< tiles

readTiles :: String -> [Tile]
readTiles [] = []
readTiles [_] = []
readTiles (i : u : xs) = case (readMaybe [i, u] :: Maybe Tile) of
  Nothing -> readTiles xs
  Just ti -> ti : readTiles xs

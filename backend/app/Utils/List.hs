module Utils.List where

import Data.List (delete, find)
import qualified Data.Set as Set

-- >>> exists [1, 1] [1, 2]
-- False
-- >>> exists [1, 1] [1, 1, 2]
-- True
-- >>> exists [] [1, 2, 3]
-- False
exists :: Eq a => [a] -> [a] -> Bool
exists [] _ = False
exists [x] ys = case find (== x) ys of
  Nothing -> False
  Just x' -> True
exists (x : xs) ys = case find (== x) ys of
  Nothing -> False
  Just x' -> exists xs (delete x' ys) && True

unique :: Ord a => [a] -> [a]
unique = Set.toList . Set.fromList

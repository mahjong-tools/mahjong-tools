{-# LANGUAGE OverloadedStrings #-}

module YesodType.TileRemaining where

import Models.Mahjong (TileRemaining (TileRemaining))
import Yesod (ToJSON (toJSON), object, (.=))

instance ToJSON TileRemaining where
  toJSON (TileRemaining (tile, remaining)) =
    object
      [ "tile" .= show tile,
        "remaining" .= remaining
      ]
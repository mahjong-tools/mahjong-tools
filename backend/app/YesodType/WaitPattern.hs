{-# LANGUAGE OverloadedStrings #-}

module YesodType.WaitPattern where

import Data.Text (pack)
import Models.Wait
  ( PairWait (PairWait),
    Wait
      ( SequenceWaitForMiddle,
        SequenceWaitForOneSide,
        SequenceWaitForTwoSides,
        TripletWait
      ),
  )
import UseCases.FindWaits
  ( WaitPattern
      ( CaseWaitForAGroupWithAPair,
        CaseWaitForAGroupWithoutPair,
        CaseWaitForAPair,
        OnlyValidGroups
      ),
  )
import Yesod (ToJSON (toJSON), object, toJsonText, (.=))

instance ToJSON Wait where
  toJSON w = case w of
    SequenceWaitForTwoSides t1 t2 wt1 wt2 ->
      object
        [ "type" .= pack "SequenceWaitForTwoSides",
          "tiles" .= (show t1 ++ show t2),
          "waits" .= (show wt1 ++ show wt2)
        ]
    SequenceWaitForOneSide t1 t2 w ->
      object
        [ "type" .= pack "SequenceWaitForOneSide",
          "tiles" .= (show t1 ++ show t2),
          "waits" .= show w
        ]
    SequenceWaitForMiddle t1 t2 w ->
      object
        [ "type" .= pack "SequenceWaitForMiddle",
          "tiles" .= (show t1 ++ show t2),
          "waits" .= show w
        ]
    TripletWait t1 t2 w ->
      object
        [ "type" .= pack "TripletWait",
          "tiles" .= (show t1 ++ show t2),
          "waits" .= show w
        ]

instance ToJSON WaitPattern where
  toJSON (CaseWaitForAGroupWithAPair w p gs) =
    object
      [ "type" .= pack "CaseWaitForAGroupWithAPair",
        "groups" .= fmap show gs,
        "pair" .= show p,
        "wait" .= toJSON w
      ]
  toJSON (CaseWaitForAPair w gs) =
    object
      [ "type" .= pack "CaseWaitForAGroupWithoutPair",
        "groups" .= fmap show gs,
        "wait"
          .= ( case w of
                 PairWait t w ->
                   object
                     [ "tiles" .= show t,
                       "wait" .= show w
                     ]
             )
      ]
  toJSON (CaseWaitForAGroupWithoutPair w gs) =
    object
      [ "groups" .= fmap show gs,
        "wait" .= toJSON w
      ]
  toJSON (OnlyValidGroups gs) = object ["groups" .= fmap show gs]
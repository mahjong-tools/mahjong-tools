{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Apis.FindWaits where

import Apis.Routes
  ( Handler,
  )
import Data.List ((\\))
import Data.Text
  ( unpack,
  )
import Models.Mahjong
  ( findRemaining,
    mahjongs,
  )
import Models.Tile
  ( Tiles (Tiles, getTiles),
    readTiles,
  )
import UseCases.FindWaits
  ( findWaits,
    retrieveWaitsFromWaitPatterns,
  )
import Yesod
  ( ToJSON (toJSON),
    Value,
    YesodRequest (reqGetParams),
    getRequest,
    object,
    (.=),
  )
import YesodType.TileRemaining ()
import YesodType.WaitPattern ()

getFindWaitsR :: Handler Value
getFindWaitsR = do
  getParameters <- reqGetParams <$> getRequest

  let tiles =
        maybe
          (Tiles [])
          (Tiles . readTiles . unpack)
          (lookup "tiles" getParameters)
  let quads =
        maybe
          (Tiles [])
          (Tiles . readTiles . unpack)
          (lookup "quads" getParameters)

  let waitPatterns = findWaits tiles
  let waits = retrieveWaitsFromWaitPatterns waitPatterns
  let remainings = (mahjongs \\ getTiles tiles) \\ (getTiles quads >>= replicate 4)
  let tileRemainings =
        findRemaining (Tiles remainings) <$> getTiles waits

  return . object $
    [ "result" .= fmap toJSON tileRemainings,
      "details" .= fmap toJSON waitPatterns
    ]

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Apis.Routes where

import Yesod
  ( RenderRoute (renderRoute),
    Yesod,
    mkYesodData,
    parseRoutes,
  )

data App = App

mkYesodData
  "App"
  [parseRoutes|
/randomSingleSuit RandomSingleSuitR GET
/findWaits FindWaitsR GET
|]

instance Yesod App
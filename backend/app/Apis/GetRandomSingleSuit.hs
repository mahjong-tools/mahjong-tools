{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Apis.GetRandomSingleSuit where

import Apis.Routes (Handler)
import Control.Concurrent.Async (withAsync)
import Data.Maybe (fromMaybe)
import Data.Text (pack, unpack)
import Models.Suit (Suit (M, P, S))
import Models.Tile (Tiles (Tiles), readTiles)
import Text.Read (readMaybe)
import UseCases.GetRandomTiles (getRandomSingleSuitTiles)
import Utils.Random (shuffle)
import Yesod
  ( MonadIO (liftIO),
    Value,
    YesodRequest (reqGetParams),
    getRequest,
    invalidArgs,
    object,
    (.=),
  )

getRandomSingleSuitR :: Handler Value
getRandomSingleSuitR = do
  getParameters <- reqGetParams <$> getRequest

  let get =
        fromMaybe
          13
          ( lookup
              "get"
              getParameters
              >>= (\x -> readMaybe $ unpack x :: Maybe Int)
          )
  suit <-
    liftIO $
      maybe
        (do head <$> shuffle [M, P, S])
        return
        ( lookup
            "suit"
            getParameters
            >>= (\x -> readMaybe $ unpack x :: Maybe Suit)
        )
  let exclude =
        maybe
          (Tiles [])
          (Tiles . readTiles . unpack)
          (lookup "exclude" getParameters)
  let given =
        maybe
          (Tiles [])
          (Tiles . readTiles . unpack)
          (lookup "given" getParameters)

  tiles <-
    liftIO $
      getRandomSingleSuitTiles
        suit
        get
        given
        exclude
  return . object $
    [ "tiles" .= pack (show tiles)
    ]

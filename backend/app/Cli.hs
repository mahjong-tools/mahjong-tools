import Models.Suit (Suit (M, P, S))
import Models.Tile (Tile (Tile), Tiles (Tiles))
import Text.Read (readMaybe)
import UseCases.FindWaits (findWaits, retrieveWaitsFromWaitPatterns)
import UseCases.GetRandomTiles (getRandomSingleSuitTiles)
import Utils.Random (shuffle)

main :: IO ()
main = do
  randomSuit <- shuffle [M, P, S]
  let suit = head randomSuit
  randomTiles <- getRandomSingleSuitTiles suit 13 (Tiles []) (Tiles [])
  putStrLn "Question:"
  putStrLn ""
  putStrLn $ "\t" ++ show randomTiles
  putStrLn ""
  putStrLn "Press enter to reveal answer..."
  getLine
  let waits = findWaits randomTiles
  case waits of
    [] -> putStrLn "No answer"
    ws -> do
      putStrLn $ "\t" ++ show (retrieveWaitsFromWaitPatterns ws)
      putStrLn ""
      putStrLn "Details:"
      mapM_ (\w -> putStrLn $ "\t" ++ show w) ws

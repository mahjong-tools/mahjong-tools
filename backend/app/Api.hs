{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

import Apis.GetRandomSingleSuit (getRandomSingleSuitR)
import Apis.Routes (App (App), Route (RandomSingleSuitR, FindWaitsR))
import System.Environment (lookupEnv)
import Yesod
  ( Html,
    RenderRoute (renderRoute),
    RepJson,
    TypedContent,
    Value,
    Yesod (defaultLayout),
    mkYesod,
    mkYesodData,
    mkYesodDispatch,
    object,
    parseRoutes,
    warp,
    whamlet,
  )
import Apis.FindWaits (getFindWaitsR)

mkYesodDispatch
  "App"
  [parseRoutes|
/randomSingleSuit RandomSingleSuitR GET
/findWaits FindWaitsR GET
|]

main :: IO ()
main = do
  maybePort <- lookupEnv "PORT"
  let port =
        case maybePort of
          Nothing -> 3000
          Just portStr -> read portStr :: Int
  warp port App
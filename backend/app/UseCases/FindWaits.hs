module UseCases.FindWaits where

import Control.Monad
  ( guard,
    join,
  )
import Data.List
  ( delete,
    intersect,
    sort,
  )
import Data.Maybe
  ( catMaybes,
  )
import qualified Data.Set as Set
import Models.Group
  ( Group
      ( Group
      ),
    Pair (Pair),
  )
import Models.Suit
  ( Suit
      ( M
      ),
  )
import Models.Tile
  ( Tile
      ( Tile
      ),
    Tiles (Tiles),
    getPrevTile,
    getSuccessorTile,
  )
import Models.Wait
  ( PairWait (PairWait),
    Wait
      ( SequenceWaitForMiddle,
        SequenceWaitForOneSide,
        SequenceWaitForTwoSides,
        TripletWait
      ),
  )
import Utils.List (exists, unique)

data WaitPattern
  = -- |
    --  In case of a pair can be formed with a waiting group when there are groups with 2 additional tiles
    CaseWaitForAGroupWithAPair Wait Pair [Group] -- 2 + 3n + 2
  | -- |
    --  In case of waiting a pair when there are groups with 1 additional tiles
    CaseWaitForAPair PairWait [Group] -- 3n + 1
  | -- |
    CaseWaitForAGroupWithoutPair Wait [Group] -- 3n + 2
  | -- |
    --  Only groups can be recognized when the number of tiles is multiple of 3
    OnlyValidGroups [Group] -- 3n
  deriving (Show, Ord, Eq)

-- >>> enumWaits [Tile (2, M), Tile (2, M)]
-- [(TripletWait 2m 2m 2m,[])]
-- >>> enumWaits [Tile (1, M), Tile (2, M)]
-- [(SequenceWaitForOneSide 1m 2m 3m,[])]
-- >>> enumWaits [Tile (2, M), Tile (3, M)]
-- [(SequenceWaitForTwoSides 2m 3m 4m 1m,[])]
-- >>> enumWaits [Tile (2, M), Tile (4, M)]
-- [(SequenceWaitForMiddle 2m 4m 3m,[])]
-- >>> enumWaits [Tile (7, M), Tile (9, M)]
-- [(SequenceWaitForMiddle 7m 9m 8m,[])]
-- >>> enumWaits [Tile (8, M), Tile (9, M)]
-- [(SequenceWaitForOneSide 8m 9m 7m,[])]
-- >>> enumWaits [Tile (2, M), Tile (2, M), Tile (3, M), Tile (4, M), Tile (6, M)]
-- [(TripletWait 2m 2m 2m,[3m,4m,6m]),(SequenceWaitForTwoSides 2m 3m 4m 1m,[2m,4m,6m]),(SequenceWaitForMiddle 2m 4m 3m,[2m,3m,6m])]
enumWaits :: [Tile] -> [(Wait, [Tile])]
enumWaits tiles = case sortedTiles of
  [] -> []
  [_] -> []
  (x : xs) -> catMaybes [a, b, c]
    where
      next = getSuccessorTile x
      nextNext = next >>= getSuccessorTile
      a = if x `elem` xs then Just (TripletWait x x x, delete x xs) else Nothing
      b = case next of
        Nothing -> Nothing
        Just y -> do
          if y `elem` xs
            then case (w1, w2) of
              (Just p, Just n) -> Just (SequenceWaitForTwoSides x y p n, delete y xs)
              (Just p, Nothing) -> Just (SequenceWaitForOneSide x y p, delete y xs)
              (Nothing, Just n) -> Just (SequenceWaitForOneSide x y n, delete y xs)
              _ -> Nothing
            else Nothing
          where
            w1 = getPrevTile x
            w2 = getSuccessorTile y
      c = case (next, nextNext) of
        (Just y, Just z) -> do
          if z `elem` xs
            then Just (SequenceWaitForMiddle x z y, delete z xs)
            else Nothing
        _ -> Nothing
  where
    sortedTiles = sort tiles

-- >>> formGroup [Tile (1, M), Tile (2, M), Tile (2, M), Tile (1, M), Tile (1, M)]
-- [(1m1m1m,[2m,2m])]
-- >>> formGroup [Tile (1, M), Tile (1, M), Tile (2, M), Tile (2, M), Tile (3, M), Tile (3, M)]
-- [(1m2m3m,[1m,2m,3m])]
-- >>> getSuccessorTile $ Tile (1, M)
-- Just 2m
-- >>> (Just $ Tile (2, M)) >>= getSuccessorTile
-- Just 3m
-- >>> [Tile (1, M), Tile (2, M), Tile (2, M), Tile (3, M), Tile (3, M)] `intersect` [Tile (2, M), Tile (3, M)]
-- [2m,2m,3m,3m]
-- >>>  [Tile (2, M), Tile (3, M)] `intersect` [Tile (1, M), Tile (2, M), Tile (2, M), Tile (3, M), Tile (3, M)]
-- [2m,3m]
-- >>> [Tile (1, M), Tile (2, M), Tile (2, M)] `intersect` [Tile (2, M), Tile (3, M)]
-- [2m,2m]
-- >>> intersect [1, 1] [1, 2]
-- [1,1]
-- >>> intersect [1, 1, 2, 2, 3] [1, 2, 2]
-- [1,1,2,2]
formGroup :: [Tile] -> [(Group, [Tile])]
formGroup tiles = case sortedTiles of
  [] -> []
  [_] -> []
  [_, _] -> []
  (x : xs) -> catMaybes [a, b]
    where
      next = getSuccessorTile x
      nextNext = next >>= getSuccessorTile
      a =
        if exists [x, x] xs
          then Just (Group x x x, delete x . delete x $ xs)
          else Nothing
      b = case (next, nextNext) of
        (Just y, Just z) ->
          if exists [y, z] xs
            then Just (Group x y z, delete y . delete z $ xs)
            else Nothing
        _ -> Nothing
  where
    sortedTiles = sort tiles

-- >>> enumGroups [Tile (1, M), Tile (1, M), Tile (1, M), Tile (2, M), Tile (2, M), Tile (2, M), Tile (3, M), Tile (3, M), Tile (3, M)]
-- [[Group 1m 1m 1m,Group 2m 2m 2m,Group 3m 3m 3m],[Group 1m 2m 3m,Group 1m 2m 3m,Group 1m 2m 3m]]
enumGroups :: [Tile] -> [[Group]]
enumGroups tiles = case formGroup tiles of
  [] -> []
  xs -> do
    (group, remainingTiles) <- xs
    case remainingTiles of
      [] -> [[group]]
      [_] -> []
      [_, _] -> []
      rs -> case enumGroups rs of
        [] -> []
        gs -> do
          g <- gs
          [group : g]

waitPair :: [Tile] -> Maybe (PairWait, [Tile])
waitPair tiles = case sort tiles of
  [] -> Nothing
  (x : xs) -> Just (PairWait x x, xs)

-- >>> formPair [Tile (3, M), Tile (3, M), Tile (3, M), Tile (4, M)]
-- Just (Pair 3m 3m,[3m,4m])
formPair :: [Tile] -> Maybe (Pair, [Tile])
formPair tiles = case sort tiles of
  [] -> Nothing
  [_] -> Nothing
  (x : xs) -> if x `elem` xs then Just (Pair x x, delete x xs) else Nothing

-- >>> findWaits $ Tiles [Tile (3, M)]
-- [CaseWaitForAPair 3m(3m) []]
-- >>> findWaits $ Tiles [Tile (3, M), Tile (3, M)]
-- [CaseWaitForAGroupWithoutPair 3m3m(3m) []]
-- >>> findWaits $ Tiles [Tile (3, M), Tile (4, M)]
-- [CaseWaitForAGroupWithoutPair 3m4m(2m5m) []]
-- >>> findWaits $ Tiles [Tile (3, M), Tile (5, M)]
-- [CaseWaitForAGroupWithoutPair 3m5m(4m) []]
-- >>> findWaits $ Tiles [Tile (3, M), Tile (3, M), Tile (3, M)]
-- [OnlyValidGroups [3m3m3m]]
-- >>> findWaits $ Tiles [Tile (3, M), Tile (3, M), Tile (4, M)]
-- []
-- >>> findWaits $ Tiles [Tile (3, M), Tile (3, M), Tile (3, M), Tile (4, M)]
-- [CaseWaitForAGroupWithAPair 3m4m(2m5m) 3m3m [],CaseWaitForAPair 4m(4m) [3m3m3m]]
-- >>> findWaits $ Tiles [Tile (2, M), Tile (2, M), Tile (2, M), Tile (3, M), Tile (3, M), Tile (3, M), Tile (4, M)]
-- [CaseWaitForAGroupWithAPair 3m4m(2m5m) 3m3m [2m2m2m],CaseWaitForAGroupWithAPair 2m4m(3m) 2m2m [3m3m3m],CaseWaitForAGroupWithAPair 2m2m(2m) 3m3m [2m3m4m],CaseWaitForAGroupWithAPair 3m3m(3m) 2m2m [2m3m4m],CaseWaitForAPair 4m(4m) [2m2m2m,3m3m3m]]
-- >>> findWaits $ Tiles [Tile (3, M), Tile (3, M), Tile (3, M), Tile (4, M), Tile (4, M)]
-- [CaseWaitForAGroupWithoutPair 4m4m(4m) [3m3m3m]]
-- >>> findWaits $ Tiles [Tile (2, M), Tile (2, M), Tile (2, M), Tile (3, M), Tile (3, M), Tile (3, M), Tile (4, M), Tile (4, M)]
-- [CaseWaitForAGroupWithoutPair 2m3m(1m4m) [2m3m4m,2m3m4m],CaseWaitForAGroupWithoutPair 4m4m(4m) [2m2m2m,3m3m3m]]
findWaits :: Tiles -> [WaitPattern]
findWaits (Tiles tiles) = case sort tiles of
  [] -> []
  [x] -> [CaseWaitForAPair (PairWait x x) []]
  [x, y] -> (\(w, _) -> CaseWaitForAGroupWithoutPair w []) <$> [(w, rs) | (w, rs) <- enumWaits [x, y], null rs]
  tiles ->
    if l `mod` 3 == 0
      then handle3n tiles
      else
        if l `mod` 3 == 1
          then unique $ handle3nPlus1 tiles
          else unique $ handle3nPlus2 tiles
    where
      l = length tiles

handle3n :: [Tile] -> [WaitPattern]
handle3n tiles = OnlyValidGroups <$> enumGroups tiles

-- >>> handle3nPlus1 [Tile (1, M), Tile (1, M), Tile (2, M), Tile (2, M), Tile (3, M), Tile (3, M), Tile (4, M), Tile (5, M), Tile (9, M), Tile (9, M)]
-- []
-- >>> enumWaits [ Tile (4, M), Tile (5, M)]
-- [(4m5m(3m6m),[])]
-- >>> formGroup [Tile (1, M), Tile (1, M), Tile (2, M), Tile (2, M), Tile (3, M), Tile (3, M)]
-- []

-- >>> delete 1 [1, 1, 2, 3]
-- [1,2,3]

handle3nPlus1 :: [Tile] -> [WaitPattern]
handle3nPlus1 tiles = do
  i <- [0 .. (length tiles - 1)]
  let h = take i tiles
  let t = drop i tiles
  join
    [ do
        case waitPair t of
          Nothing -> []
          Just (p, rs) -> do
            let remaining = h ++ rs
            CaseWaitForAPair p <$> enumGroups remaining,
      do
        case formPair t of
          Nothing -> []
          Just (p, rs) -> do
            let remaining = h ++ rs
            j <- [0 .. (length remaining - 1)]
            let h' = take j remaining
            let t' = drop j remaining
            case enumWaits t' of
              [] -> []
              waits -> do
                (wait, rs') <- waits
                case h' ++ rs' of
                  [] -> return $ CaseWaitForAGroupWithAPair wait p []
                  rs'' -> CaseWaitForAGroupWithAPair wait p <$> enumGroups rs''
    ]

handle3nPlus2 :: [Tile] -> [WaitPattern]
handle3nPlus2 tiles = do
  i <- [0 .. (length tiles - 1)]
  let h = take i tiles
  let t = drop i tiles
  case enumWaits t of
    [] -> []
    waits -> do
      (wait, rs) <- waits
      case h ++ rs of
        [] -> return $ CaseWaitForAGroupWithoutPair wait []
        remaining -> CaseWaitForAGroupWithoutPair wait <$> enumGroups remaining

retrieveWaits :: WaitPattern -> [Tile]
retrieveWaits (CaseWaitForAGroupWithAPair wait _ _) = case wait of
  (SequenceWaitForTwoSides _ _ a b) -> [a, b]
  (SequenceWaitForOneSide _ _ a) -> [a]
  (SequenceWaitForMiddle _ _ a) -> [a]
  (TripletWait _ _ a) -> [a]
retrieveWaits (CaseWaitForAPair (PairWait _ a) _) = [a]
retrieveWaits (CaseWaitForAGroupWithoutPair wait _) = case wait of
  (SequenceWaitForTwoSides _ _ a b) -> [a, b]
  (SequenceWaitForOneSide _ _ a) -> [a]
  (SequenceWaitForMiddle _ _ a) -> [a]
  (TripletWait _ _ a) -> [a]
retrieveWaits _ = []

retrieveWaitsFromWaitPatterns :: [WaitPattern] -> Tiles
retrieveWaitsFromWaitPatterns = Tiles . Set.toList . foldl (\s wp -> Set.fromList (retrieveWaits wp) `Set.union` s) Set.empty

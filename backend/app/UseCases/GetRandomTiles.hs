module UseCases.GetRandomTiles where

import Control.Monad (forM)
import Data.Array.IO
  ( IOArray,
    newListArray,
    readArray,
    writeArray,
  )
import Data.List (sort)
import Models.Mahjong (tilesOfASuit)
import Models.Suit (Suit)
import Models.Tile
  ( Tile,
    Tiles
      ( Tiles
      ),
  )
import Utils.Random (shuffle)

getRandomSingleSuitTiles :: Suit -> Int -> Tiles -> Tiles -> IO Tiles
getRandomSingleSuitTiles suit count (Tiles given) (Tiles exclude) = do
  randomized <- shuffle . filter (`notElem` given) . filter (`notElem` exclude) $ tilesOfASuit suit
  return . Tiles . sort $ given ++ take count randomized
